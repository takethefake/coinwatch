async function sellpriceReverse() {
  let sellprices = await lrangeAsync("sellprice", 0, -1);
  sellprices.reverse();
  sellprices = sellprices.map(parseFloat);
  return sellprices;
}
async function keyReverse(key) {
  let sellprices = await lrangeAsync(key, 0, -1);
  sellprices.reverse();
  sellprices = sellprices.map(parseFloat);
  return sellprices;
}

async function sellpriceDiff() {
  let sellprices = await sellpriceReverse();
  diff = [];
  for (i = 0; i < sellprices.length; i++) {
    diff.push(sellprices[i] - sellprices[i - 1]);
  }
  return diff;
}
async function coinbaseSellCost(amount) {
  buyprice = await keyReverse("buyprice");
  sellprice = await keyReverse("sellprice");
  return (buyprice[0] - sellprice[0]) * amount + amount * 0.015;
}
async function coinbaseSellReturn(amount) {
  buyprice = await keyReverse("buyprice");
  sellprice = await keyReverse("sellprice");
  return sellprice[0] * amount + amount * 0.015;
}

async function coinbaseBuyCost(amount) {}
async function coinbaseBuyReturn(amount) {
  buyprice = await keyReverse("buyprice");
  return amount * 0.979 / buyprice[0];
}

async function current() {
  sellprices = await sellpriceDiff();
  return sellprices.reduce((prev, c) => prev + c, 0);
}
