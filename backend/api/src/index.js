import Koa from "koa";
import Router from "koa-router";
import logger from "koa-logger";
import RedisLib from "redis";
import Promise from "bluebird";

Promise.promisifyAll(RedisLib.RedisClient.prototype);
Promise.promisifyAll(RedisLib.Multi.prototype);

const redis = RedisLib.createClient(6379, "redis");

const koa = new Koa();
const app = new Router();

app.get("/", async ctx => {
  ctx.body = `GET /price

  Returns the Etherumprices in Euro

  Example Response
  [
    {
      "buy": 659.3,
      "sell": 645.78,
      "time": 1518126381
  },
  ...
  ]

  Queryparams:
    first=<number> : returns the first <number> entries

    last=<number> : returns the last <number> entries
    
    (from=<timestamp>)?\&(to=<timestamp>)? : returns all entries between a specified timestamp`;
});

app.get("/price", async ctx => {
  const query = ctx.request.query;
  let prices = null;
  if (query.last) {
    prices = await redis.zrevrangebyscoreAsync(
      "prices",
      "+inf",
      "0",
      "LIMIT",
      0,
      query.last
    );
  } else if (query.first) {
    prices = await redis.zrangebyscoreAsync(
      "prices",
      "0",
      "+inf",
      "LIMIT",
      0,
      query.first
    );
  } else {
    prices = await redis.zrangebyscoreAsync(
      "prices",
      query.from || "0",
      query.to || "+inf"
    );
  }
  prices = prices.map(JSON.parse);

  ctx.body = prices;
});

koa.use(logger());
koa.use(app.routes());
koa.listen(process.env.APP_PORT || 3000, err => {
  if (err) console.log(err);
  console.log(`🌴  Koa server listen on ` + (process.env.APP_PORT || 3000));
});
