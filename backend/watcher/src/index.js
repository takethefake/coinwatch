import * as cbLib from "coinbase";
import RedisLib from "redis";
import Promise from "bluebird";

Promise.promisifyAll(RedisLib.RedisClient.prototype);
Promise.promisifyAll(RedisLib.Multi.prototype);
Promise.promisifyAll(cbLib);

// THIS KEY IS NOT SENSITIVE use key with permission for wallet:user:email
// ONLY NEEDED FOR GETPRICES
var coinbase = new cbLib.Client({
  apiKey: "8Z4srm4JxMywMNHI",
  apiSecret: "g1qPk4by87W0wYcIthZZUpoRYC6eFHAg"
});

const redis = RedisLib.createClient(6379, "redis");

async function checkPrices() {
  try {
    const buyPrice = await coinbase.getBuyPriceAsync({
      currencyPair: "ETH-EUR"
    });
    const sellPrice = await coinbase.getSellPriceAsync({
      currencyPair: "ETH-EUR"
    });
    let current = {
      buy: parseFloat(buyPrice.data.amount),
      sell: parseFloat(sellPrice.data.amount),
      time: Math.floor(Date.now() / 1000)
    };
    await redis.zaddAsync("prices", current.time, JSON.stringify(current));
    console.log(current);
  } catch (e) {
    console.log(e);
  }
  setTimeout(checkPrices, 30000);
}

checkPrices();
